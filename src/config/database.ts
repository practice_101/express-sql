import { DataSource,DatabaseType } from "typeorm"

const DB = new DataSource({
    type: process.env.DB_CONNECTION as DatabaseType,
    host: process.env.DB_HOST,
    //@ts-ignore
    port: process.env.DB_PORT,
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    entities: ["src/models/*{.ts,.js}"],
    synchronize:true
})

DB.initialize()
    .then(() => {
        console.log("MYSQL connected!")
    })
    .catch((err) => {
        console.error("Error during MYSQL initialization", err)
    })


export default DB;
