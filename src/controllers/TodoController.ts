import { validate } from 'class-validator';
import { Request, Response } from 'express';
import DB from '../config/database';
import { TodoDto } from '../dto/todoDto';
import Todo from '../models/Todo';

const todoRepo = DB.getRepository(Todo);

export const createTodo = async (req:Request, res:Response) => {
    try {
       const { task } = req.body;
       const todoDto = new TodoDto()
        todoDto.task = task
        const errors = await validate(todoDto);
        if (errors.length > 0) {
          res.status(422).send(errors);
        }
        const todoData = {task:task,user_id:req.user.id}     
        const todoEntity = todoRepo.create(todoData);
        const data = await todoRepo.save(todoEntity);
        res.send(data);
    } catch (error) {
        return res.status(400).send(error.message);
    }
}

export const getTodo = async (req:Request, res:Response) => {
    try {
         const user = req['user'];
         const data = await todoRepo.findBy({user_id:user.id});
         if(data.length == 0) res.send({message:'no todo please create todo first'});
         res.send(data);
     } catch (error) {
         return res.status(400).send(error.message);
     }
}
