import { validate } from 'class-validator';
import { Request, Response } from 'express';
import DB from '../config/database';
import { LoginDto } from '../dto/loginDto';
import { RegisterDto } from '../dto/registerDto';
import { checkHash } from '../lib/hash';
import { generateToken } from '../lib/jwt';
import User from '../models/User';

const userRepo = DB.getRepository(User);

export const register = async (req:Request, res:Response) => {
    try {
        const {name, email, password} = req.body;
        const registerDTO = new RegisterDto()
        registerDTO.email = email; registerDTO.name = name; registerDTO.password=password;
        const errors = await validate(registerDTO);
        if (errors.length > 0) {
          res.status(422).send(errors);
        }
        const userEntity = userRepo.create(req.body);
        const user = await userRepo.save(userEntity);
        delete user['password'];
        const token = generateToken({user:user});
        user['token'] = token;
        res.send(user);
    } catch (error) {
        res.status(400).send(error.message)
    }
}

export const authUser = async (req:Request, res:Response) => {
    try {
        res.send(req.user);   
    } catch (error) {
        res.send(error.message);
    }
}

export const login = async (req:Request, res:Response) => {
    try {
        const {email, password} = req.body;
        const loginDto = new LoginDto()
        loginDto.email = email;
        loginDto.password = password;
        const errors = await validate(loginDto);
        if (errors.length > 0) {
          res.status(422).send(errors);
        } 
        let getUser = await userRepo.findOneBy({email:email});
        if(!getUser) throw new Error('email not found');
        const loginAttempt = await checkHash(password,getUser.password);
        if(!loginAttempt) throw new Error('incorrect password');
        delete getUser.password
        const token = generateToken({user:getUser});
        getUser['token'] = token;
        res.send(getUser);   
    } catch (error) {
        res.status(400).send({message:error.message});
    }
}