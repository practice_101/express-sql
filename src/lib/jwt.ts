import jsonwebtoken from "jsonwebtoken";

export const generateToken = (data:any) => {
    return jsonwebtoken.sign(data, process.env.JWT_SECRET, { expiresIn: "2h" });
}


export const verifyToken = (token : string) => {
    return jsonwebtoken.verify(token,process.env.JWT_SECRET);
}
