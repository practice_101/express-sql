import { BeforeInsert, Column, Entity, PrimaryGeneratedColumn } from "typeorm"
import { hasher } from "../lib/hash"

@Entity()
export default class User {
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    name: string

    @Column()
    email: string

    @Column()
    password: string

    @BeforeInsert()
    async hashPassword(){
        this.password = await hasher(this.password);
    }
}