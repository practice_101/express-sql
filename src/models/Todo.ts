import {Column, Entity, PrimaryGeneratedColumn } from "typeorm"

@Entity()
export default class Todo {
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    user_id:number

    @Column()
    task: string

}