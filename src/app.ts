import * as dotenv from 'dotenv'
dotenv.config();
import express, { Request, Response } from 'express';
import { createTodo, getTodo } from './controllers/TodoController';
import { authUser, login, register } from './controllers/UserController';
import { authenticate } from './middleware/AuthMiddleware';


const app = express()
app.use(express.json())


app.get('/',(req:Request,res:Response) => res.send('express-app'));
app.post('/register',register);
app.post('/login',login);
app.use('/auth/*',authenticate);
app.get('/auth/user',authUser),
app.post('/auth/todo',createTodo);
app.get('/auth/todo',getTodo);






app.listen(5050);