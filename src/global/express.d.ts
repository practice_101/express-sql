import { IAuser } from "../types/user";

declare global {
    namespace Express {
      export interface Request {
        user?: IAuser;
      }
    }
  }

