import { NextFunction, Request, Response } from 'express';
import { verifyToken } from "../lib/jwt";

export function authenticate(req:Request, res:Response, next:NextFunction) {
    const authHeader = req.headers['authorization']

    const token = authHeader && authHeader.split(' ')[1]

    
    if (token == null) return res.sendStatus(401)

    try {
        const user = verifyToken(token);
        req.user = user; 
        next()
    } catch (error) {
        console.log(error);
        return res.sendStatus(401)
    }

}